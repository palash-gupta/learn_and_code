class ExceptionHandler {

  readonly MONGOCLIENT_OBJECT_EXCEPTION = "Unable to create mongoclient object";
  readonly DATABASE_OBJECT_EXCEPTION = "Unable to create database object";
  readonly CLIENT_NOT_ADDED_TO_TOPIC_QUEUE = "Unable to add client to topic queue";
  readonly MESSAGE_NOT_FETCHED_FROM_QUEUE = "Unable to get messages from queue";
  readonly CLIENT_NOT_FOUND_BY_TOPIC_ID = "Unable to get client by topic id";
  readonly TOPIC_QUEUE_DETAILS_NOT_FOUND = "Unable to fetch topic queue details";
  readonly TOPIC_QUEUE_MAPPING_NOT_CREATED = "Unable to create topic queue mapping";
  readonly TOPIC_DETAILS_NOT_FOUND_BY_NAME = "Unable to get topic getails by name";
  readonly UNABLE_TO_FIND_DATA = "Unable to access find query in database";
  readonly CLIENT_NOT_ADDED_TO_ROOT_COLLECTION = "Unable to add client to root collection";
  readonly UNABEL_TO_FETCH_TOPIC_LIST = "Unable to fetch topics list";
  readonly MESSAGE_NOT_PUBLISHED_IN_QUEUE = "Unable to publish message in queue";
  readonly MESSAGE_NOT_PUBLISHED_IN_DATABASE = "Unable to publish message in database";
  readonly READ_MESSAGES_NOT_SAVED = "Unable to save read messages";
  readonly READ_MESSAGES_NOT_FETCHED = "Unable to get read messages";
  readonly SERVER_NOT_STARTED = "Unable to initialize server, with PORT";

  public handle(promise) {
    return promise
      .then((data) => [data, undefined])
      .catch((error) => Promise.resolve([undefined, error]));
  }
}
export let exceptionHandler = new ExceptionHandler();