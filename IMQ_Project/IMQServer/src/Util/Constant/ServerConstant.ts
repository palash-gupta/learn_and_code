const PORT: number = 1234;
const HTTP = "http";
const WELCOME_MESSAGE = "Welcome";
const MAX_CONNECTIONS = 5;
const SOURCE_URI = "localhost";
const DESTINATION_URI = "192.168.0.181";

export {
  PORT,
  HTTP,
  WELCOME_MESSAGE,
  MAX_CONNECTIONS,
  SOURCE_URI,
  DESTINATION_URI,
};