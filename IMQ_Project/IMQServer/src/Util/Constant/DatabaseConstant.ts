const ROOT_COLLECTION = "RootCollection";
const CLIENT_COLLECTION = "ClientCollection";
const ADMIN_COLLECTION = "admin";
const MONGO_CONNECTION_URL: string = "mongodb://localhost:27017/";
const DATABASE_NAME: string = "IMQ_DATABASE";
const LOCALHOST = "127.0.0.1";

export {
  ROOT_COLLECTION,
  CLIENT_COLLECTION,
  MONGO_CONNECTION_URL,
  DATABASE_NAME,
  LOCALHOST,
  ADMIN_COLLECTION
};
