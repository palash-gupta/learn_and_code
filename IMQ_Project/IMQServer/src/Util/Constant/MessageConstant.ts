// ADMIN CONSTANTS
const ADMIN_DETAILS_MATCHED = "Admin Details Matched";
const ADMIN_DETAILS_NOT_MATCHED = "Admin Details Not Matched";

// USER CONSTANTS
const INVALID_USER_ID = "Invalid User ID";
const USER_ALREADY_EXISTS = "User Already Exists";
const USER_ADDED = "User Added";

// DATABASE CONSTANT MESSAGE
const DATABASE_CONNECTED = "Database Connected";

// SERVER MESSAGES
const SERVER_STARTED = "Server Started";

// TOPIC QUEUE CONSTANTS
const TOPIC_ALREADY_EXISTS = "Topic already exist, please enter some other name";

// COMMON CONSTANTS
const COMMA = ",";
const EXISTS = "exists";

export {
  ADMIN_DETAILS_MATCHED,
  ADMIN_DETAILS_NOT_MATCHED,
  INVALID_USER_ID,
  USER_ALREADY_EXISTS,
  USER_ADDED,
  DATABASE_CONNECTED,
  COMMA,
  EXISTS,
  SERVER_STARTED,
  TOPIC_ALREADY_EXISTS
};