import { server } from "./Server/Connection";
import * as dotenv from "dotenv";
import { DatabaseConnection } from "./Database/Connection";
import { logger } from "./Log/Log4js";
import { exceptionHandler } from "./Util/ExceptionHandler";
import { PORT } from "./Util/Constant/ServerConstant";
import { DeadLetterQueue } from "./Server/Services/DeadLetterQueue";

dotenv.config();

(async () => {
  try {
    let [data, error] = await exceptionHandler.handle(
      server.initializeServer()
    );
    if (error)
      throw new Error(`${exceptionHandler.SERVER_NOT_STARTED} ${PORT}`);
    [data, error] = await exceptionHandler.handle(
      DatabaseConnection.getInstance().createConnection()
    );
    if (error) throw error;
    let deadLetterQueue = DeadLetterQueue.getObject();
    deadLetterQueue.checkMessageExpiryTime();
  } catch (error) {
    logger.error(error.message);
  }
})();