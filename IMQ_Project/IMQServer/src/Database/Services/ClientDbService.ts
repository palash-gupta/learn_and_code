import { ROOT_COLLECTION } from "../../Util/Constant/DatabaseConstant";
import { USER_ADDED } from "../../Util/Constant/MessageConstant";
import { exceptionHandler } from "../../Util/ExceptionHandler";
import { DatabaseConnection } from "../Connection";

class ClientDbService {
  private dbConnectionObject: DatabaseConnection;
  constructor() {
    this.dbConnectionObject = DatabaseConnection.getInstance();
  }

  public async addClientToRootCollection(data) {
    if (data.serialId === null || data.serialId === undefined) {
      return "Undefined or null Id";
    }
    let [clientCount, clientError] = await exceptionHandler.handle(
      this.findClient(data.serialId)
    );
    if (clientError) throw new Error(exceptionHandler.UNABLE_TO_FIND_DATA);
    if (clientCount.length == 0) {
      await this.dbConnectionObject.database
        .collection(ROOT_COLLECTION)
        .insertOne({ user_id: data.serialId });
      return USER_ADDED;
    }
  }

  public async findClient(user_id: string) {
    if (user_id === undefined || user_id === null) {
      return "Undefined or null Id";
    } else {
      return await this.dbConnectionObject.database
        .collection(ROOT_COLLECTION)
        .find({ user_id }, { _id: 0 })
        .toArray();
    }
  }
}

export let clientDbService = new ClientDbService();
