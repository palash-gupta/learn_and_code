import { DatabaseConnection } from "../Connection";
import { v4 as uuidv4 } from "uuid";
import { QueueService } from "../../Server/Services/Queue";
import {
  TOPIC_QUEUE_MAP_COLLECTION,
  QUEUE_ID,
  READ_MESSAGES_COLLECTION,
} from "../../Util/Constant/TopicQueueConstant";
import { logger } from "../../Log/Log4js";
import { exceptionHandler } from "../../Util/ExceptionHandler";
import { TOPIC_ALREADY_EXISTS } from "../../Util/Constant/MessageConstant";

class TopicQueueDbService {
  private message: string;
  private imqQueue: QueueService;
  private dbConnectionObject: DatabaseConnection;

  constructor() {
    this.imqQueue = QueueService.getObject();
    this.dbConnectionObject = DatabaseConnection.getInstance();
  }

  async getAllTopicsList() {
    return await this.dbConnectionObject.database
      .collection(TOPIC_QUEUE_MAP_COLLECTION)
      .find({}, { topicName: 1, _id: 0 })
      .toArray();
  }

  async createTopicQueueMapper([topicName, queueName]) {
    let [topic, topicError] = await exceptionHandler.handle(
      this.getTopicDetailByName(topicName)
    );
    if (topicError)
      throw new Error(
        `${exceptionHandler.TOPIC_DETAILS_NOT_FOUND_BY_NAME}: ${topicName}`
      );
    if (topic.length == 0) {
      let [data, error] = await exceptionHandler.handle(
        this.addTopicQueue(topicName, queueName)
      );
      logger.info(data);
      if (error)
        throw new Error(exceptionHandler.TOPIC_QUEUE_MAPPING_NOT_CREATED);
      this.message = `${topicName} Topic and ${queueName} Queue created`;
    } else {
      this.message = `${topicName} ${TOPIC_ALREADY_EXISTS}`;
    }
    return this.message;
  }

  public async addTopicQueue(topicName: string, queueName: string) {
    if (
      topicName === undefined ||
      topicName === null ||
      queueName === undefined ||
      queueName === null
    ) {
      return "Undefined or null topicname, queuename";
    }
    await this.dbConnectionObject.database
      .collection(TOPIC_QUEUE_MAP_COLLECTION)
      .insertOne({
        topicName,
        topicId: uuidv4(),
        queueId: QUEUE_ID,
        queueName,
      });
    return "Topic Queue Details Added";
  }

  public async getTopicDetailByName(topicName: string) {
    if(topicName === undefined || topicName === null) {
      return "Undefined or null Id";
    }
    return await this.dbConnectionObject.database
      .collection(TOPIC_QUEUE_MAP_COLLECTION)
      .find({ topicName }, { _id: 0 })
      .toArray();
  }

  public async subscribeToTopic(data) {
    let [
      { topicId, queueId, queueName },
      error,
    ] = await exceptionHandler.handle(this.getTopicQueueDetails(data.message));
    if (error) throw new Error(exceptionHandler.TOPIC_QUEUE_DETAILS_NOT_FOUND);
    let [client, clientError] = await exceptionHandler.handle(
      this.getClientByTopicId(queueName, topicId, data.serialId)
    );
    if (clientError)
      throw new Error(exceptionHandler.CLIENT_NOT_FOUND_BY_TOPIC_ID);
    if (client.length == 0) {
      [client, clientError] = await exceptionHandler.handle(
        this.addClientToTopicQueue(queueName, topicId, data.serialId, queueId)
      );
      if (clientError)
        throw new Error(exceptionHandler.CLIENT_NOT_ADDED_TO_TOPIC_QUEUE);
      logger.info(client);
    }
    let [messages, messageError] = await exceptionHandler.handle(
      this.imqQueue.getAllMessages(data.serialId, topicId)
    );
    if (messageError)
      throw new Error(exceptionHandler.MESSAGE_NOT_FETCHED_FROM_QUEUE);
    return messages;
  }

  private async getTopicQueueDetails(topicName: string) {
    return await this.dbConnectionObject.database
      .collection(TOPIC_QUEUE_MAP_COLLECTION)
      .findOne({ topicName }, { _id: 0, topicName: 0 });
  }

  private async getClientByTopicId(
    queueName: string,
    topicId: string,
    user_id: string
  ) {
    return await this.dbConnectionObject.database
      .collection(queueName)
      .find({ topicId, user_id })
      .toArray();
  }

  private async addClientToTopicQueue(
    queueName: string,
    topicId: string,
    user_id: string,
    queueId: string
  ) {
    await this.dbConnectionObject.database.collection(queueName).insertOne({
      queueId,
      topicId,
      user_id,
    });
    return "Client Mapped With Topic And Queue";
  }

  public async publishMessageInQueue(data, [topicMessage, topicName]) {
    let result = await this.dbConnectionObject.database
      .collection(TOPIC_QUEUE_MAP_COLLECTION)
      .findOne({ topicName }, { topicId: 1, queueId: 1, _id: 0 });
    this.imqQueue.add(result, data);
    return topicMessage;
  }

  public async publishMessageInDatabase(data, [topicMessage, topicName]) {
    let [
      { topicId, queueId, queueName },
      error,
    ] = await exceptionHandler.handle(this.getTopicQueueDetails(topicName));
    if (error) throw new Error(exceptionHandler.TOPIC_QUEUE_DETAILS_NOT_FOUND);

    let [client, clientError] = await exceptionHandler.handle(
      this.getClientByTopicId(queueName, topicId, data.serialId)
    );
    if (clientError)
      throw new Error(exceptionHandler.CLIENT_NOT_FOUND_BY_TOPIC_ID);

    if (client.length == 1) {
      let messages: string[];
      if (client[0].messages == undefined) {
        messages = [topicMessage];
      } else {
        messages = [...client[0].messages, topicMessage];
      }
      await this.updateMessagesInTopic(
        queueName,
        topicId,
        data.serialId,
        messages
      );
    } else {
      await this.dbConnectionObject.database
        .collection(`${queueName}`)
        .insertOne({
          topicId,
          queueId,
          user_id: data.serialId,
          messages: [topicMessage],
        });
    }
  }

  private async updateMessagesInTopic(
    queueName: string,
    topicId: string,
    user_id: string,
    messages: string[]
  ) {
    await this.dbConnectionObject.database
      .collection(`${queueName}`)
      .updateOne({ topicId, user_id }, { $set: { messages: messages } });
  }

  public async getReadMessages(user_id: string, topicId: string) {
    return await this.dbConnectionObject.database
      .collection(READ_MESSAGES_COLLECTION)
      .find({ user_id, topicId }, { msgIds: 1, _id: 0 })
      .toArray();
  }

  public async saveReadMessages(ids: string[], id: string, topicId: string) {
    await this.dbConnectionObject.database
      .collection(READ_MESSAGES_COLLECTION)
      .insert({ topicId, user_id: id, msgIds: [...ids] });
    return "Seen messages saved in read collection";
  }
}

export let topicQueueDbService = new TopicQueueDbService();
