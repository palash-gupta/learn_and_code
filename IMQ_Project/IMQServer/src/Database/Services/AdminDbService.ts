import { ADMIN_COLLECTION } from "../../Util/Constant/DatabaseConstant";
import { DatabaseConnection } from "../Connection";

class AdminDbService {

  private dbConnectionObject: DatabaseConnection;
  constructor() {
    this.dbConnectionObject = DatabaseConnection.getInstance();
  }
  async validateAdmin(id) {
    if (id === undefined || id === null) {
      return "Undefined or null Id";
    }
    return (
      await this.dbConnectionObject.database
        .collection(ADMIN_COLLECTION)
        .find({ id: id })
        .toArray()
    ).length;
  }
}

export let adminDbservice = new AdminDbService();