import { MongoClient } from "mongodb";
import { logger } from "../Log/Log4js";
import {
  MONGO_CONNECTION_URL,
  DATABASE_NAME,
} from "../Util/Constant/DatabaseConstant";
import { DATABASE_CONNECTED } from "../Util/Constant/MessageConstant";
import { exceptionHandler } from "../Util/ExceptionHandler";

export class DatabaseConnection {
  
  private static dbConnection: DatabaseConnection;
  public database;
  private constructor() {}

  public static getInstance(): DatabaseConnection {
    if (
      DatabaseConnection.dbConnection === null ||
      DatabaseConnection.dbConnection === undefined
    ) {
      DatabaseConnection.dbConnection = new DatabaseConnection();
    }
    return DatabaseConnection.dbConnection;
  }

  public async createConnection() {
    let dbObjectError: Error;
    if (this.database == undefined || this.database == null) {
      let [mongoClient, mongoClientError] = await exceptionHandler.handle(
        this.getMongoClientObject(MONGO_CONNECTION_URL)
      );
      if (mongoClientError)
        throw new Error(exceptionHandler.MONGOCLIENT_OBJECT_EXCEPTION);
      [this.database, dbObjectError] = await exceptionHandler.handle(
        this.getDatabaseObject(mongoClient)
      );
      if (dbObjectError)
        throw new Error(exceptionHandler.DATABASE_OBJECT_EXCEPTION);
      logger.info(DATABASE_CONNECTED);
    }
    return this.database;
  }

  private async getMongoClientObject(url: string) {
    return await MongoClient.connect(url);
  }

  private async getDatabaseObject(mongoClient: MongoClient) {
    return mongoClient.db(DATABASE_NAME);
  }
}
