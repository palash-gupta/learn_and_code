import { v4 as uuidv4 } from "uuid";
import { topicQueueDbService } from "../../Database/Services/TopicQueueDbService";
import { logger } from "../../Log/Log4js";
import { COMMA } from "../../Util/Constant/MessageConstant";
import { exceptionHandler } from "../../Util/ExceptionHandler";

export class QueueService {
  public queueMessages: any[];
  private static imqQueue: QueueService;
  constructor() {
    this.queueMessages = [];
  }

  public static getObject() {
    if (this.imqQueue == null || this.imqQueue == undefined) {
      this.imqQueue = new QueueService();
    }
    return this.imqQueue;
  }

  add(user, data) {
    let result = this.queueMessages.filter(
      (item) => item.user_id === data.serialId && item.topicId == user.topicId
    );
    if (result.length == 0) {
      this.queueMessages.push(
        Object.assign(user, {
          user_id: data.serialId,
          messages: [
            {
              msgId: uuidv4(),
              msg: data.message.split(COMMA)[0],
              createdTime: new Date(),
              expiryTime: new Date(
                new Date().setDate(new Date().getDate() + 1)
              ),
            },
          ],
        })
      );
    } else {
      let messageList = result[0].messages;
      if (messageList == undefined) {
        Object.assign(result[0], {
          messages: [
            {
              msgId: uuidv4(),
              msg: data.message.split(COMMA)[0],
              createdTime: new Date(),
              expiryTime: new Date(
                new Date().setDate(new Date().getDate() + 1)
              ),
            },
          ],
        });
      } else {
        Object.assign(result[0], {
          messages: [
            ...messageList,
            {
              msgId: uuidv4(),
              msg: data.message.split(COMMA)[0],
              createdTime: new Date(),
              expiryTime: new Date(
                new Date().setDate(new Date().getDate() + 1)
              ),
            },
          ],
        });
      }
    }
  }

  async getAllMessages(userId, topicId) {
    let list = this.getMessagesByTopicId(topicId);
    let ids: string[] = list.map((obj) => obj.msgId);
    let [msgIdList, error] = await exceptionHandler.handle(
      topicQueueDbService.getReadMessages(userId, topicId)
    );
    if (error) throw new Error(exceptionHandler.READ_MESSAGES_NOT_FETCHED);
    let readMessages = [].concat(...msgIdList.map((msg) => msg.msgIds));
    if (readMessages.length == 0) {
      let [data, error] = await exceptionHandler.handle(
        topicQueueDbService.saveReadMessages(ids, userId, topicId)
      );
      if (error) throw new Error(exceptionHandler.READ_MESSAGES_NOT_SAVED);
      logger.info(data);
      return list;
    } else {
      for (let row = 0; row < readMessages.length; row++) {
        for (let column = 0; column < list.length; column++) {
          if (readMessages[row] == list[column].msgId) {
            Object.assign(list[column], { read: "y" });
            break;
          }
        }
      }
      let unseenMessages = list.filter((obj) => obj.read !== "y");
      let unseenMessagesIds = unseenMessages.map((obj) => obj.msgId);
      let [data, error] = await exceptionHandler.handle(
        topicQueueDbService.saveReadMessages(unseenMessagesIds, userId, topicId)
      );
      if (error) throw new Error(exceptionHandler.READ_MESSAGES_NOT_SAVED);
      logger.info(data);
      return unseenMessages;
    }
  }

  private getMessagesByTopicId(topicId: string) {
    return [].concat(
      ...this.queueMessages
        .filter((item) => item.topicId == topicId)
        .map((item) => item.messages)
    );
  }
}