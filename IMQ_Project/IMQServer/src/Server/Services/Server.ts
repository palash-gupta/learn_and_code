import { logger } from "../../Log/Log4js";
import { RequestType } from "imq-request-response-enum";
import { ResponseProtocol } from "response-protocol";
import { requestHandler } from "../Handlers/Request";

class ServerService {

  getClientDetails(socket) {
    return {
      ip: socket.remoteAddress,
      port: socket.remotePort,
    };
  }

  addClientConnection(connection) {
    let clientDetails = this.getClientDetails(connection);
    connection.on("data", async function (body) {
      let data = JSON.parse(body);
      logger.info(data);
      let response: ResponseProtocol;
      switch (data.requestType) {
        case RequestType.NEW_USER:
          response = await requestHandler.handleNewUserRequest(data);
          connection.write(JSON.stringify(response));
          break;
        case RequestType.EXISTING_USER:
          response = await requestHandler.handleExistingUserRequest(data);
          connection.write(JSON.stringify(response));
          break;
        case RequestType.VIEW_TOPICS:
          response = await requestHandler.handleViewTopicsRequest(data);
          connection.write(JSON.stringify(response));
          break;
        case RequestType.PUBLISH_MESSAGE:
          response = await requestHandler.handlePublishMessageRequest(data);
          connection.write(JSON.stringify(response));
          break;
        case RequestType.SUBSCRIBE_TOPIC:
          response = await requestHandler.handleSubscribeTopicRequest(data);
          connection.write(JSON.stringify(response));
          break;
        case RequestType.VERIFY_ADMIN:
          response = await requestHandler.handleVerifyAdminRequest(data);
          connection.write(JSON.stringify(response));
          break;
        case RequestType.CREATE_TOPIC_QUEUE:
          response = await requestHandler.handleCreateTopicRequest(data);
          connection.write(JSON.stringify(response));
          break;
        default:
          break;
      }
    });

    connection.on("end", () => {
      logger.info(`Client disconnected with port no. ${clientDetails.port}`);
    });

    connection.on("error", () => {
      logger.info(`Something went wrong with port no. ${clientDetails.port}`);
    });
  }
}
export let serverService = new ServerService();