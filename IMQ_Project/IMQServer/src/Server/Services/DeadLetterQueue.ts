import { TIMER } from "../../Util/Constant/TopicQueueConstant";
import { QueueService } from "./Queue";

export class DeadLetterQueue {

  private static deadLetterQueue: DeadLetterQueue;
  private imqQueue: QueueService;
  private deadMessages: any[];
  private constructor() {
    this.deadMessages = [];
    this.imqQueue = QueueService.getObject();
  }

  public static getObject() {
    if (this.deadLetterQueue == null || this.deadLetterQueue == undefined) {
      this.deadLetterQueue = new DeadLetterQueue();
    }
    return this.deadLetterQueue;
  }

  public checkMessageExpiryTime() {
    setInterval(() => {
      this.imqQueue.queueMessages = this.imqQueue.queueMessages.filter(
        (queueObject) => {
          let unexpiredMessages = [];
          queueObject.messages.filter((message) => {
            if (message.expiryTime > new Date()) {
              unexpiredMessages.push(message);
            } else {
              this.deadMessages.push(message);
            }
          });
          Object.assign(queueObject, { messages: unexpiredMessages });
          return queueObject;
        }
      );
    }, TIMER);
  }
}