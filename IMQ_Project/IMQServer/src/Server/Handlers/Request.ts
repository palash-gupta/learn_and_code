import { ResponseType } from "imq-request-response-enum";
import { ResponseProtocol } from "response-protocol";
import { adminDbservice } from "../../Database/Services/AdminDbService";
import { clientDbService } from "../../Database/Services/ClientDbService";
import { topicQueueDbService } from "../../Database/Services/TopicQueueDbService";
import { logger } from "../../Log/Log4js";
import { WELCOME_MESSAGE } from "../../Util/Constant/ServerConstant";
import {
  ADMIN_DETAILS_MATCHED,
  ADMIN_DETAILS_NOT_MATCHED,
  COMMA,
  EXISTS,
  INVALID_USER_ID,
  USER_ALREADY_EXISTS,
} from "../../Util/Constant/MessageConstant";
import { exceptionHandler } from "../../Util/ExceptionHandler";
import { responseHandler } from "./Response";

export class RequestHandler {
  private client;
  private response: ResponseProtocol;
  private message: string;
  private messages: string[];

  async handleNewUserRequest(data) {
    try {
      let clientError: Error;
      [this.client, clientError] = await exceptionHandler.handle(
        clientDbService.findClient(data.serialId)
      );
      if (clientError) throw new Error(exceptionHandler.UNABLE_TO_FIND_DATA);
      if (this.client.length == 0) {
        let [message, error] = await exceptionHandler.handle(
          clientDbService.addClientToRootCollection(data)
        );
        if (error)
          throw new Error(exceptionHandler.CLIENT_NOT_ADDED_TO_ROOT_COLLECTION);
        this.response = responseHandler.getResponseBody(
          ResponseType.USER_ADDED,
          message,
          data.serialId
        );
      } else {
        this.response = responseHandler.getResponseBody(
          ResponseType.USER_EXISTS,
          USER_ALREADY_EXISTS,
          data.serialId
        );
      }
      return this.response;
    } catch (error) {
      logger.error(error.message);
    }
  }

  async handleExistingUserRequest(data) {
    try {
      let clientError: Error;
      [this.client, clientError] = await exceptionHandler.handle(
        clientDbService.findClient(data.serialId)
      );
      if (clientError) throw new Error(exceptionHandler.UNABLE_TO_FIND_DATA);
      if (this.client.length == 0) {
        this.response = responseHandler.getResponseBody(
          ResponseType.CLIENT_NOT_FOUND,
          INVALID_USER_ID,
          data.serialId
        );
      } else {
        this.response = responseHandler.getResponseBody(
          ResponseType.USER_FOUND,
          WELCOME_MESSAGE,
          data.serialId
        );
      }
      return this.response;
    } catch (error) {
      logger.error(error.message);
    }
  }

  async handleViewTopicsRequest(data) {
    try {
      let [topics, error] = await exceptionHandler.handle(
        topicQueueDbService.getAllTopicsList()
      );
      if (error) throw new Error(exceptionHandler.UNABEL_TO_FETCH_TOPIC_LIST);
      this.response = responseHandler.getResponseBody(
        ResponseType.LIST_TOPICS,
        JSON.stringify(topics),
        data.serialId
      );
      return this.response;
    } catch (error) {
      logger.error(error.message);
    }
  }

  async handlePublishMessageRequest(data) {
    try {
      let error: Error;
      [this.message, error] = await exceptionHandler.handle(
        topicQueueDbService.publishMessageInQueue(
          data,
          data.message.split(COMMA)
        )
      );
      if (error)
        throw new Error(exceptionHandler.MESSAGE_NOT_PUBLISHED_IN_QUEUE);
      [this.message, error] = await exceptionHandler.handle(
        topicQueueDbService.publishMessageInDatabase(
          data,
          data.message.split(COMMA)
        )
      );
      if (error)
        throw new Error(exceptionHandler.MESSAGE_NOT_PUBLISHED_IN_DATABASE);
      this.response = responseHandler.getResponseBody(
        ResponseType.MESSAGE_PUBLISHED,
        this.message,
        data.serialId
      );
      return this.response;
    } catch (error) {
      logger.error(error.message);
    }
  }

  async handleSubscribeTopicRequest(data) {
    try {
      let error: Error;
      [this.messages, error] = await exceptionHandler.handle(
        topicQueueDbService.subscribeToTopic(data)
      );
      if (error) throw error;
      this.response = responseHandler.getResponseBody(
        ResponseType.TOPIC_SUBSCRIBED,
        JSON.stringify(this.messages),
        data.serialId
      );
      return this.response;
    } catch (error) {
      logger.error(error.message);
    }
  }

  async handleVerifyAdminRequest(data) {
    try {
      let [isAdmin, error] = await exceptionHandler.handle(
        adminDbservice.validateAdmin(data.serialId)
      );
      if (error) throw new Error(exceptionHandler.UNABLE_TO_FIND_DATA);
      if (isAdmin) {
        this.response = responseHandler.getResponseBody(
          ResponseType.ADMIN_VERIFIED,
          ADMIN_DETAILS_MATCHED,
          data.serialId
        );
      } else {
        this.response = responseHandler.getResponseBody(
          ResponseType.UNAUTHORIZED_ADMIN,
          ADMIN_DETAILS_NOT_MATCHED,
          data.serialId
        );
      }
      return this.response;
    } catch (error) {
      logger.error(error.message);
    }
  }

  async handleCreateTopicRequest(data) {
    try {
      let error: Error;
      [this.message, error] = await exceptionHandler.handle(
        topicQueueDbService.createTopicQueueMapper(data.message.split(COMMA))
      );
      if (error) throw error;
      if (this.message.includes(EXISTS)) {
        this.response = responseHandler.getResponseBody(
          ResponseType.TOPIC_ALREADY_EXISTS,
          this.message,
          data.serialId
        );
      } else {
        this.response = responseHandler.getResponseBody(
          ResponseType.TOPIC_QUEUE_CREATED,
          this.message,
          data.serialId
        );
      }
      return this.response;
    } catch (error) {
      logger.error(error.message);
    }
  }
}
export let requestHandler = new RequestHandler();
