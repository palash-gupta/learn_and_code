import { ResponseProtocol } from "response-protocol";
import { SOURCE_URI, DESTINATION_URI } from '../../Util/Constant/ServerConstant';

class ResponseHandler {

  private response: ResponseProtocol;

  getResponseBody(type: string, message: string, id: string) {
    this.response = {
      responseType: type,
      message: message,
      serialId: id,
      sourceUri: SOURCE_URI,
      destinationUri: DESTINATION_URI,
      messageTime: new Date(),
    };
    return this.response;
  }
}
export let responseHandler = new ResponseHandler();