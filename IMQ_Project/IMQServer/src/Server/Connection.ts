import { MAX_CONNECTIONS, PORT } from "../Util/Constant/ServerConstant";
import { serverService } from "./Services/Server";
import { logger } from "../Log/Log4js";
import * as net from "net";
import { SERVER_STARTED } from "../Util/Constant/MessageConstant";

class Server {
  async initializeServer() {
      let server = net.createServer((connection) => {
        serverService.addClientConnection(connection);
      });
      server.maxConnections = MAX_CONNECTIONS;
      server.listen(PORT, () => {
        logger.info(`Running server on port ${PORT}`);
      });
      return SERVER_STARTED;
  }
}
export let server = new Server();