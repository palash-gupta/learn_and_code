const gulp = require('gulp');
const terser = require('gulp-terser');
const ts = require('gulp-typescript');

function server() {
    return gulp.src('src/**/*.ts')
        .pipe(ts())
        .pipe(terser())
        .pipe(gulp.dest('dist'));
};

exports.default = server;