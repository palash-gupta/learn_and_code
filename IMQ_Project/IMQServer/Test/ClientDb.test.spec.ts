import { expect } from "chai";
import { clientDbService } from "../src/Database/Services/ClientDbService";
import { DatabaseConnection } from "../src/Database/Connection";
import { adminDbservice } from "../src/Database/Services/AdminDbService";

describe("Client Database Tests", () => {
  let dbConnectionObject;
  before(async () => {
    let databaseObject = DatabaseConnection.getInstance();
    dbConnectionObject = await databaseObject.createConnection();
  });

  it("should add client to root collection if it does not exist", async () => {
    let id = Math.floor(Math.random() * 10000);
    await clientDbService.addClientToRootCollection({ serialId: `${id}` });
    let client = await clientDbService.findClient(`${id}`);
    expect(client[0].user_id).to.equal(`${id}`);
  });

  it("should find the user by ID", async () => {
    let id = "1234";
    await clientDbService.addClientToRootCollection({ serialId: id });
    let client = await clientDbService.findClient(id);
    expect(client[0].user_id).to.equal(id);
  });

  it("should get zero values in result array", async () => {
    let id = "";
    let client = await clientDbService.findClient(id);
    expect(client.length).to.equal(0);
  });

  it("should return undefined value in find client result array", async () => {
    let id = undefined;
    let client = await clientDbService.findClient(id);
    expect(client).to.equal("Undefined or null Id");
  });

  it("should return null value in find client result array", async () => {
    let id = null;
    let client = await clientDbService.findClient(id);
    expect(client).to.equal("Undefined or null Id");
  });

  it("should return undefined value in root collection result array", async () => {
    let id = undefined;
    let client = await clientDbService.addClientToRootCollection({
      serialId: id,
    });
    expect(client).to.equal("Undefined or null Id");
  });

  it("should return null value in root collection result array", async () => {
    let id = null;
    let client = await clientDbService.addClientToRootCollection({
      serialId: id,
    });
    expect(client).to.equal("Undefined or null Id");
  });

  it("should return undefined value in admin result array", async () => {
    let id = undefined;
    let client = await adminDbservice.validateAdmin(id);
    expect(client).to.equal("Undefined or null Id");
  });

  it("should return null value in admin result array ", async () => {
    let id = null;
    let client = await adminDbservice.validateAdmin(id);
    expect(client).to.equal("Undefined or null Id");
  });

  it("should return zero value in admin result array ", async () => {
    let client = await adminDbservice.validateAdmin({});
    expect(client).to.equal(0);
  });
});