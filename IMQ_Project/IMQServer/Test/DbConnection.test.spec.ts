import { expect } from "chai";
import { DatabaseConnection } from "../src/Database/Connection";
import { DATABASE_NAME } from "../src/Util/Constant/DatabaseConstant";

describe("Database Connection Tests", () => {
  let databaseObject;
  let dbConnectionObject;

  before(async () => {
    databaseObject = DatabaseConnection.getInstance();
    dbConnectionObject = await databaseObject.createConnection();
  });

  it("should check database connection object created", async () => {
    expect(dbConnectionObject).to.instanceOf(Object);
  });
  
  it("should check database name is IMQ_database", async () => {
    expect(dbConnectionObject.s.databaseName).to.equal(DATABASE_NAME);
  });
});