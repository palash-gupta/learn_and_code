import { expect } from "chai";
import { DatabaseConnection } from "../src/Database/Connection";
import { adminDbservice } from "../src/Database/Services/AdminDbService";

describe("Admin Database Tests", () => {
  
  let dbConnectionObject;
  before(async () => {
    let databaseObject = DatabaseConnection.getInstance();
    dbConnectionObject = await databaseObject.createConnection();
  });
  it("should return null value in admin result array ", async () => {
    let id = null;
    let client = await adminDbservice.validateAdmin(id);
    expect(client).to.equal("Undefined or null Id");
  });

  it("should return zero value in admin result array ", async () => {
    let client = await adminDbservice.validateAdmin({});
    expect(client).to.equal(0);
  });
});