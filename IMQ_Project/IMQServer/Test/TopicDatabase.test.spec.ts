import { expect } from "chai";
import { topicQueueDbService } from "../src/Database/Services/TopicQueueDbService";

describe("Topic Queue Database Tests", () => {
  
  let resultString: string;

  it("should return length greater than or equal to 0", async () => {
    let topics = await topicQueueDbService.getAllTopicsList();
    expect(topics.length).to.gte(0);
  });

  it("should return message topic and queue created", async () => {
    let topic_name = `Topic${Math.floor(Math.random() * 1000)}`;
    let queue_name = `Queue${Math.floor(Math.random() * 1000)}`;
    let message = await topicQueueDbService.createTopicQueueMapper([
      topic_name,
      queue_name,
    ]);
    expect(message).to.equal(
      `${topic_name} Topic and ${queue_name} Queue created`
    );
  });

  it("should add topic with queue name", async () => {
    let result = await topicQueueDbService.addTopicQueue("Palash", "gupta");
    expect(result).to.equal("Topic Queue Details Added");
  });

  it("should return null or undefined string", async () => {
    let result = await topicQueueDbService.addTopicQueue(null, null);
    expect(result).to.equal("Undefined or null topicname, queuename");
  });

  it("should return topic detail by name", async () => {
    let topic = await topicQueueDbService.getTopicDetailByName("Palash");
    expect(topic[0].topicName).to.equal("Palash");
  });

  it("should return null or undefined string", async () => {
    resultString = await topicQueueDbService.getTopicDetailByName(null);
    expect(resultString).to.equal("Undefined or null Id");
  });

  it("should return null or undefined string", async () => {
    resultString = await topicQueueDbService.getTopicDetailByName(undefined);
    expect(resultString).to.equal("Undefined or null Id");
  });
});