import * as readline from "readline";

class Readline {
  private _readLine: readline.Interface;
  async getInputFromConsole(query) {
    this._readLine = this.getRealineObject();
    return new Promise((resolve) =>
      this._readLine.question(query, (ans: string) => {
        resolve(ans);
        this._readLine.close();
      })
    );
  }

  getRealineObject() {
    const readLine = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
      terminal: false,
    });
    return readLine;
  }
}

export let readlineObject = new Readline();
