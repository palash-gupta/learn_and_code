export class CommandValidator {

  validatePublishSubscribeCommand = (userInputArray: string[], choices: string[]) => {
    return (
      userInputArray.length == 0 ||
      userInputArray[0] != "imq" ||
      userInputArray[1] == undefined ||
      !choices.includes(userInputArray[1])
    );
  };

  validatePublishMessageCommand = (userInputArray: string[], topicName: string) => {
    return (
      userInputArray[2] == undefined ||
      userInputArray[2] != topicName ||
      userInputArray[3] == undefined ||
      userInputArray[3] != "-m" ||
      userInputArray[4] == undefined
    );
  };

  validateSubscribeTopicCommand = (userInputArray: string[], topicName: string) => {
    return userInputArray[2] == undefined || userInputArray[2] != topicName;
  };

  validateTopicChangeCommand = (userInputArray: string[]) => {
    return userInputArray[2] == undefined || userInputArray[2] != "topic";
  };

  validateImqConnectCommand = (userInputArray: string[]) => {
    return userInputArray[0] === "imq" && userInputArray[1] === "connect";
  };

  validateExistingClientCommand = (userInputArray: string[]) => {
    return (
      this.validateImqConnectCommand(userInputArray) &&
      userInputArray[2] != undefined
    );
  };

  validateNewClientCommand = (userInputArray: string[]) => {
    return userInputArray[2] == undefined || userInputArray[2].length == 0;
  };

  validateCreateTopicCommand = (adminInputArray: string[]) => {
    return (
      adminInputArray[0] == "imq" &&
      adminInputArray[1] == "create" &&
      adminInputArray[2] != undefined
    );
  };

  validateEnterIdCommand = (inputArray: string[]) => {
    return (
      this.validateImqConnectCommand(inputArray) && inputArray[2] != undefined
    );
  };
}

export let command = new CommandValidator();