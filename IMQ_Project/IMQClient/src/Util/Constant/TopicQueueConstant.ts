// CLIENT FILES CONSTANTS
const ENTER_USER_TYPE_COMMAND_STRING = `\n--> Enter imq connect new-client or existing-client\n\n-->`;
const ENTER_TOPIC_NAME_COMMAND_STRING =
  "\n--> Enter imq connect topic-name : \n\n-->";
const PUBLISH_SUBSCRIBE_CHANGE_TOPIC_COMMAND_STRING = `
                                        \n--> Enter "imq publish topic-name -m <message>" to publish a message
                                        \n--> Enter "imq subscribe topic-name" to subscribe a topic
                                        \n--> Enter "imq change topic" to change topic\n\n-->`;
const NO_NEW_MESSAGE_AVAILABLE_IN_TOPIC =
  "\n\n-- No new messages available in this topic --\n";

// ADMIN FILES CONSTANT
const CREATE_TOPIC_COMMAND_STRING = `\n-->Enter imq create topic-name\n\n-->`;
const CREATE_TOPIC_OR_EXIT_COMMAND_STRING = `Want to create more topic? 
                                            \nPress 1 to create more topic
                                            \nPress 2 to exit\n `;

// COMMON CONSTANT FOR BOTH CLIENT AND ADMIN
const ENTER_ID_COMMAND_STRING = "\n--> Enter imq connect your-id\n\n-->";

export {
  ENTER_ID_COMMAND_STRING,
  ENTER_USER_TYPE_COMMAND_STRING,
  ENTER_TOPIC_NAME_COMMAND_STRING,
  PUBLISH_SUBSCRIBE_CHANGE_TOPIC_COMMAND_STRING,
  NO_NEW_MESSAGE_AVAILABLE_IN_TOPIC,
  CREATE_TOPIC_COMMAND_STRING,
  CREATE_TOPIC_OR_EXIT_COMMAND_STRING,
};