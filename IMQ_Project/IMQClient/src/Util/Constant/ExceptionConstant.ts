export const UNABLE_TO_CONNECT_SERVER: string = "Unable to connect server or service has not started yet";
export const UNDEFINED_OR_NULL_ID = "Undefined or null Id";