const PORT: number = 1234;
const PUBLISH = "publish";
const SUBSCRIBE = "subscribe";
const CHANGE = "change";
const EXIT_STRINGS: string[] = ["end", "close", "exit"];
const SPACE = " ";
const SELECT_ONE_TOPIC = "\n Select 1 topic from below list :\n";
const NO_TOPIC_AVAILABLE = "No topic available in queue";
const SELECTED_TOPIC = "\n Topic Selected : ";
const MESSAGES = "\n\nMessages : \n";
const SOURCE_URI = "localhost";
const DESTINATION_URI = "192.168.0.181";
const THANK_YOU = "Thank you";
const INVALID_COMMAND = "\n\nInvalid command : Please enter again\n";

export {
  PORT,
  PUBLISH,
  SUBSCRIBE,
  CHANGE,
  EXIT_STRINGS,
  SPACE,
  SELECT_ONE_TOPIC,
  SELECTED_TOPIC,
  MESSAGES,
  NO_TOPIC_AVAILABLE,
  SOURCE_URI,
  THANK_YOU,
  DESTINATION_URI,
  INVALID_COMMAND
};