import { RequestProtocol } from "request-protocol";
import { SOURCE_URI, DESTINATION_URI } from "./Constant/MessageConstant";

class RequestHandler {

  requestBody: RequestProtocol;

  getRequestBody(type: string, message: string, id: string) {
    this.requestBody = {
      requestType: type,
      message: message,
      serialId: id,
      sourceUri: SOURCE_URI,
      destinationUri: DESTINATION_URI,
      messageTime: new Date(),
    };
    return this.requestBody;
  }
}
export let requestHandler = new RequestHandler();