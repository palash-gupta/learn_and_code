export enum UserType {
  EXISTING_CLIENT = "existing-client",
  NEW_CLIENT = "new-client"
}