import * as net from "net";
import { logger } from "../Log/Log4js";
import { PORT } from "../Util/Constant/MessageConstant";
import { UNABLE_TO_CONNECT_SERVER } from "../Util/Constant/ExceptionConstant";
import { adminHandler } from "./Handler";

export class AdminConnection {
  
  public connection: net.Socket;

  public async createConnection() {
    try {
      this.connection = net.connect({ port: PORT });
      await adminHandler.handleAdminEvents();
      return this.connection;
    } catch (error) {
      logger.error(UNABLE_TO_CONNECT_SERVER);
    }
  }
}

export let admin = new AdminConnection();
admin.createConnection();