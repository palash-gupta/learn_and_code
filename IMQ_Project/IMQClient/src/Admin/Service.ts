import { RequestType } from "imq-request-response-enum";
import { RequestProtocol } from "request-protocol";
import { requestHandler } from "../Util/RequestHandler";
import { logger } from "../Log/Log4js";
import { readlineObject } from "../Readline/Readline";
import { command } from "../Util/CommandValidator";
import { SPACE } from "../Util/Constant/MessageConstant";
import { INVALID_COMMAND } from "../Util/Constant/MessageConstant";
import {
  CREATE_TOPIC_COMMAND_STRING,
  ENTER_ID_COMMAND_STRING,
} from "../Util/Constant/TopicQueueConstant";

class AdminService {
  
  private requestBody: RequestProtocol;
  private topicName: string;
  private queueName: string = "IMQ_QUEUE";
  private id: string;
  private adminInputArray: string[];

  async getAdminInput(instructions: string) {
    return (await readlineObject.getInputFromConsole(instructions))
      .toString()
      .trim()
      .toLowerCase()
      .split(SPACE);
  }

  public async getTopicName() {
    this.adminInputArray = await this.getAdminInput(CREATE_TOPIC_COMMAND_STRING);
    if (command.validateCreateTopicCommand(this.adminInputArray)) {
      return this.adminInputArray[2];
    } else {
      logger.info(INVALID_COMMAND);
      await this.getTopicName();
    }
  }

  public async getAdminId() {
    this.adminInputArray = await this.getAdminInput(ENTER_ID_COMMAND_STRING);
    if (command.validateEnterIdCommand(this.adminInputArray)) {
      return this.adminInputArray[2];
    } else {
      logger.info(INVALID_COMMAND);
      await this.getAdminId();
    }
  }

  public async getAdminDetail() {
    this.id = await this.getAdminId();
    this.requestBody = requestHandler.getRequestBody(
      RequestType.VERIFY_ADMIN,
      "",
      this.id
    );
    return this.requestBody;
  }

  public async createTopicQueue() {
    this.topicName = await this.getTopicName();
    this.requestBody = requestHandler.getRequestBody(
      RequestType.CREATE_TOPIC_QUEUE,
      `${this.topicName},${this.queueName}`,
      this.id
    );
    return this.requestBody;
  }
}

export let adminService = new AdminService();