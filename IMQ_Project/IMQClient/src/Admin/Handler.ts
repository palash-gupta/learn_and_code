import { logger } from "../Log/Log4js";
import { readlineObject } from "../Readline/Readline";
import { adminService } from "./Service";
import { CREATE_TOPIC_OR_EXIT_COMMAND_STRING } from "../Util/Constant/TopicQueueConstant";
import { RequestType, ResponseType } from "imq-request-response-enum";
import { requestHandler } from "../Util/RequestHandler";
import { admin } from "./Main";
import { RequestProtocol } from "request-protocol";
import { ResponseProtocol } from "response-protocol";
import { THANK_YOU } from "../Util/Constant/MessageConstant";

class AdminHandler {

  private responseBody: ResponseProtocol;
  private requestBody: RequestProtocol;

  async handleAdminEvents() {
    this.requestBody = await adminService.getAdminDetail();
    admin.connection.write(JSON.stringify(this.requestBody));
    admin.connection.on("data", async (data) => {
      this.responseBody = JSON.parse(data.toString());
      logger.info(this.responseBody);
      this.handleActionsByResponseType(this.responseBody.responseType);
    });
    admin.connection.on("error", (error: Error) => {
      logger.error(error.message);
    });
    admin.connection.on("close", () => {
      logger.info("Connection has been closed");
    });
  }

  async handleActionsByResponseType(responseType) {
    switch (responseType) {
      case ResponseType.ADMIN_VERIFIED:
        this.requestBody = await adminService.createTopicQueue();
        admin.connection.write(JSON.stringify(this.requestBody));
        break;
      case ResponseType.UNAUTHORIZED_ADMIN:
        const adminId = await adminService.getAdminId();
        this.requestBody = requestHandler.getRequestBody(RequestType.VERIFY_ADMIN, "", adminId);
        admin.connection.write(JSON.stringify(this.requestBody));
        break;
      case ResponseType.TOPIC_QUEUE_CREATED:
        await this.handleCreateTopicTask();
        break;
      case ResponseType.TOPIC_ALREADY_EXISTS:
        await this.handleCreateTopicTask();
        break;
      default:
        break;
    }
  }

  async handleCreateTopicTask() {
    let choice = Number(await readlineObject.getInputFromConsole(CREATE_TOPIC_OR_EXIT_COMMAND_STRING));
    switch (choice) {
      case 1:
        this.requestBody = await adminService.createTopicQueue();
        admin.connection.write(JSON.stringify(this.requestBody));
        break;
      case 2:
        logger.info(THANK_YOU);
        admin.connection.end();
        break;
      default:
        logger.info("Invalid choice");
        admin.connection.end();
        break;
    }
  }
}
export let adminHandler = new AdminHandler();