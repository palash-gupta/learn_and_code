import * as net from "net";
import { logger } from "../Log/Log4js";
import { PORT } from "../Util/Constant/MessageConstant";
import { clientEvents } from "./Event";
import { clientHandler } from "./Handler";

export let client = net.connect({ port: PORT }, () => {
  logger.info("Connected to server!");
  clientEvents.initializeEvents();
  clientHandler.checkClientType();
});