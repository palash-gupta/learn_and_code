import { logger } from "../Log/Log4js";
import { client } from "./Main";
import { clientHandler } from "./Handler";
import { ResponseType } from "imq-request-response-enum";
import { ResponseProtocol } from "response-protocol";

class ClientEvents {

  initializeEvents() {
    client.on("data", (data) => {
      let response: ResponseProtocol = JSON.parse(data.toString());
      logger.info(`Response ${data.toString()}`);
      switch (response.responseType) {
        case ResponseType.USER_ADDED:
          clientHandler.viewTopics();
          break;
        case ResponseType.LIST_TOPICS:
          clientHandler.listAllTopics(response);
          clientHandler.takeClientInput(response);
          break;
        case ResponseType.USER_EXISTS:
          clientHandler.checkClientType();
          break;
        case ResponseType.CLIENT_NOT_FOUND:
          clientHandler.checkClientType();
          break;
        case ResponseType.USER_FOUND:
          clientHandler.viewTopics();
          break;
        case ResponseType.MESSAGE_PUBLISHED:
          clientHandler.takeClientChoice();
          break;
        case ResponseType.TOPIC_SUBSCRIBED:
          clientHandler.listMessages(JSON.parse(response.message));
          clientHandler.takeClientChoice();
          break;
        default:
          break;
      }
    });
    client.on("error", (error: Error) => {
      logger.error(error.message);
    });
    client.on("close", () => {
      logger.info("Connection has been closed");
    });
  }
}
export let clientEvents = new ClientEvents();