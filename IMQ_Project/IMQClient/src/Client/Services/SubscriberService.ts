import { RequestType } from "imq-request-response-enum";
import { RequestProtocol } from "request-protocol";
import { client } from "../Main";
import { requestHandler } from "../../Util/RequestHandler";

export class SubscriberService {
  private static subscriber: SubscriberService;
  private requestBody: RequestProtocol;

  public static getObject() {
    if (!(this.subscriber instanceof SubscriberService)) {
      this.subscriber = new SubscriberService();
    }
    return this.subscriber;
  }

  subscribeToTopic(selectedTopic: string, id: string) {
    this.requestBody = requestHandler.getRequestBody(
      RequestType.SUBSCRIBE_TOPIC,
      selectedTopic,
      id
    );
    return this.requestBody;
  }
}