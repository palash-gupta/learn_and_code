import { RequestType } from "imq-request-response-enum";
import { RequestProtocol } from "request-protocol";
import { readlineObject } from "../../Readline/Readline";
import { EXIT_STRINGS } from "../../Util/Constant/MessageConstant";
import { client } from "../Main";
import { requestHandler } from "../../Util/RequestHandler";
import { UNDEFINED_OR_NULL_ID } from "../../Util/Constant/ExceptionConstant";

export class PublisherService {

  private static publisher: PublisherService;
  private requestBody: RequestProtocol;

  public static getObject() {
    if (!(this.publisher instanceof PublisherService)) {
      this.publisher = new PublisherService();
    }
    return this.publisher;
  }

  public publishMessage(selectedTopic: string, text: string, id: string) {
    if (EXIT_STRINGS.includes(text)) {
      readlineObject.getRealineObject().close();
    } else {
      let data: string = `${text},${selectedTopic}`;
      this.requestBody = requestHandler.getRequestBody(
        RequestType.PUBLISH_MESSAGE,
        data,
        id
      );
      return this.requestBody;
    }
  }
}