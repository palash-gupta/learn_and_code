import { client } from "./Main";
import { logger } from "../Log/Log4js";
import { readlineObject } from "../Readline/Readline";
import {
  ENTER_ID_COMMAND_STRING,
  ENTER_TOPIC_NAME_COMMAND_STRING,
  ENTER_USER_TYPE_COMMAND_STRING,
  NO_NEW_MESSAGE_AVAILABLE_IN_TOPIC,
  PUBLISH_SUBSCRIBE_CHANGE_TOPIC_COMMAND_STRING,
} from "../Util/Constant/TopicQueueConstant";
import { command } from "../Util/CommandValidator";
import {
  CHANGE,
  MESSAGES,
  NO_TOPIC_AVAILABLE,
  PUBLISH,
  SELECTED_TOPIC,
  SELECT_ONE_TOPIC,
  SPACE,
  SUBSCRIBE,
} from "../Util/Constant/MessageConstant";
import { INVALID_COMMAND } from "../Util/Constant/MessageConstant";
import { PublisherService } from "./Services/PublisherService";
import { SubscriberService } from "./Services/SubscriberService";
import { UserType } from "../Enums/UserType";
import { RequestProtocol } from "request-protocol";
import { SelectedTopic } from "../Modal/SelectedTopic";
import { RequestType } from "imq-request-response-enum";
import { requestHandler } from "../Util/RequestHandler";

class ClientHandler {
  
  private id: string;
  private requestBody: RequestProtocol;
  private selectedTopic: SelectedTopic[];

  async getClientInput(instructions: string) {
    return (await readlineObject.getInputFromConsole(instructions))
      .toString()
      .trim()
      .toLowerCase()
      .split(SPACE);
  }

  public async checkClientType() {
    let userInputArray: string[] = await this.getClientInput(
      ENTER_USER_TYPE_COMMAND_STRING
    );
    if (command.validateImqConnectCommand(userInputArray)) {
      switch (userInputArray[2]) {
        case UserType.EXISTING_CLIENT:
          this.doExistingClientTask();
          break;
        case UserType.NEW_CLIENT:
          this.doNewClientTask();
          break;
        default:
          console.log(INVALID_COMMAND);
          clientHandler.checkClientType();
          break;
      }
    } else {
      logger.info(INVALID_COMMAND);
      clientHandler.checkClientType();
    }
  }

  async doExistingClientTask() {
    let userInputArray = await this.getClientInput(ENTER_ID_COMMAND_STRING);
    if (command.validateExistingClientCommand(userInputArray)) {
      this.id = userInputArray[2];
      this.requestBody = requestHandler.getRequestBody(
        RequestType.EXISTING_USER,
        "",
        this.id
      );
      client.write(JSON.stringify(this.requestBody));
    } else {
      console.log(INVALID_COMMAND);
      clientHandler.checkClientType();
    }
  }

  async doNewClientTask() {
    let userInputArray = await this.getClientInput(ENTER_ID_COMMAND_STRING);
    if (command.validateImqConnectCommand(userInputArray)) {
      if (command.validateNewClientCommand(userInputArray)) {
        console.log(INVALID_COMMAND);
        clientHandler.checkClientType();
      } else {
        this.id = userInputArray[2];
        this.requestBody = requestHandler.getRequestBody(
          RequestType.NEW_USER,
          "",
          this.id
        );
        client.write(JSON.stringify(this.requestBody));
      }
    } else {
      console.log(INVALID_COMMAND);
      clientHandler.checkClientType();
    }
  }

  viewTopics() {
    this.requestBody = requestHandler.getRequestBody(
      RequestType.VIEW_TOPICS,
      "",
      this.id
    );
    client.write(JSON.stringify(this.requestBody));
  }

  async takeClientInput(data) {
    let userInputArray = await this.getClientInput(ENTER_TOPIC_NAME_COMMAND_STRING);
    if (command.validateImqConnectCommand(userInputArray)) {
      this.selectedTopic = this.getSelectedTopic(userInputArray[2], data);
      if (this.selectedTopic.length != 0) {
        logger.info(SELECTED_TOPIC, this.selectedTopic[0].topicName);
        this.takeClientChoice();
      } else {
        logger.info(INVALID_COMMAND);
        this.takeClientInput(data);
      }
    } else {
      logger.info(INVALID_COMMAND);
      this.takeClientInput(data);
    }
  }

  async takeClientChoice() {
    let choices = ["publish", "subscribe", "change"];
    let userInputArray = await this.getClientInput(
      PUBLISH_SUBSCRIBE_CHANGE_TOPIC_COMMAND_STRING
    );
    let topicName = this.selectedTopic[0].topicName;

    if (command.validatePublishSubscribeCommand(userInputArray, choices)) {
      console.log(INVALID_COMMAND);
      this.takeClientChoice();
    } else {
      switch (userInputArray[1]) {
        case PUBLISH:
          let publisher = PublisherService.getObject();
          if (command.validatePublishMessageCommand(userInputArray, topicName)) {
            logger.info(INVALID_COMMAND);
            this.takeClientChoice();
          } else {
            this.requestBody = publisher.publishMessage(
              topicName,
              userInputArray[4],
              this.id
            );
            client.write(JSON.stringify(this.requestBody));
          }
          break;
        case SUBSCRIBE:
          let subscriber = SubscriberService.getObject();
          if (command.validateSubscribeTopicCommand(userInputArray, topicName)) {
            logger.info(INVALID_COMMAND);
            this.takeClientChoice();
          } else {
            this.requestBody = subscriber.subscribeToTopic(topicName, this.id);
            client.write(JSON.stringify(this.requestBody));
          }
          break;
        case CHANGE:
          if (command.validateTopicChangeCommand(userInputArray)) {
            logger.info(INVALID_COMMAND);
            this.takeClientChoice();
          } else {
            this.viewTopics();
          }
          break;
        default:
          logger.info(INVALID_COMMAND);
          this.takeClientChoice();
          break;
      }
    }
  }

  listMessages(messages) {
    if (messages.length != 0) {
      logger.info(MESSAGES);
      let index = 1;
      messages = messages.reverse();
      messages.forEach((object) => {
        console.log(`${index} ${object.msg}`);
        index += 1;
      });
    } else {
      logger.info(NO_NEW_MESSAGE_AVAILABLE_IN_TOPIC);
    }
  }

  listAllTopics(data) {
    let topics = JSON.parse(data.message);
    if (topics.length != 0) {
      console.log(SELECT_ONE_TOPIC);
      topics.forEach((topic, index: number) => {
        console.log(`${index + 1} --> ${topic.topicName} \n`);
      });
    } else {
      logger.info(NO_TOPIC_AVAILABLE);
      client.end();
    }
  }

  getSelectedTopic(input: string, data) {
    let topics = JSON.parse(data.message);
    return topics.filter((topic) => topic.topicName == input);
  }
}
export let clientHandler = new ClientHandler();