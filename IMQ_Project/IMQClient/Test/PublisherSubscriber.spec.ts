import { expect } from "chai";
import { RequestType } from "imq-request-response-enum";
import { PublisherService } from "../src/Client/Services/PublisherService";
import { SubscriberService } from "../src/Client/Services/SubscriberService";

describe("PUBLISHER SUBSCRIBER TESTS", () => {
  it("should should check request type and id", () => {
    let publisherService = PublisherService.getObject();
    let requestBody = publisherService.publishMessage(
      "food",
      "icecream",
      "123"
    );
    expect(requestBody.serialId).to.equal("123");
    expect(requestBody.requestType).to.equal(RequestType.PUBLISH_MESSAGE);
  });

  it("should check request type and id", () => {
    let subsciber = SubscriberService.getObject();
    let requestBody = subsciber.subscribeToTopic("food", "123");
    expect(requestBody.serialId).to.equal("123");
    expect(requestBody.requestType).to.equal(RequestType.SUBSCRIBE_TOPIC);
  });
});
